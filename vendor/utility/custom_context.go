package utility

import (
	"github.com/jinzhu/gorm"
	"github.com/labstack/echo"
)

// CustomContext ...
type CustomContext struct {
	echo.Context
	DB *gorm.DB
}

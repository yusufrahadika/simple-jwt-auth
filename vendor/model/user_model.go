package model

import (
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

// User ...
type User struct {
	Username string `gorm:"type:varchar(100);primary_key" json:"username" form:"username" validate:"omitempty,required,min=4,max=32"`
	Name     string `gorm:"type:varchar(100);not null" json:"name" form:"name" validate:"required,min=3,max=100"`
	Email    string `gorm:"type:varchar(255);unique;not null" json:"email" form:"email" validate:"omitempty,required,email,max=255"`
	Password string `gorm:"type:varchar(60);not null" json:"password" form:"password" validate:"required,min=8,max=60"`
	CustomModel
}

// BeforeSave ...
func (user *User) BeforeSave(scope *gorm.Scope) (err error) {
	if pw, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.MinCost); err == nil {
		scope.SetColumn("Password", pw)
	}
	return
}

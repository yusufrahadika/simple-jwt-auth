package model

import "time"

// CustomModel ...
type CustomModel struct {
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

package controller

import (
	"fmt"
	"model"
	"net/http"
	"strings"
	"time"
	"utility"

	"github.com/dgrijalva/jwt-go"

	"golang.org/x/crypto/bcrypt"

	"github.com/go-playground/validator"

	"github.com/labstack/echo"
)

// IndexUser ...
func IndexUser(c echo.Context) error {
	var users []model.User
	if err := (c.(*utility.CustomContext)).DB.Find(&users).Error; err != nil {
		return c.JSON(http.StatusNotFound, "")
	}
	return c.JSON(http.StatusOK, users)
}

// GetUser ...
func GetUser(c echo.Context) error {
	var user model.User
	if err := (c.(*utility.CustomContext)).DB.First(&user).Error; err != nil {
		return c.JSON(http.StatusNotFound, "")
	}
	return c.JSON(http.StatusOK, user)
}

// CreateUser ...
func CreateUser(c echo.Context) error {
	user := new(model.User)
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusNotAcceptable, err)
	}

	if err := c.Validate(user); err != nil {
		errors := []map[string]string{}
		for _, err := range err.(validator.ValidationErrors) {
			errors = append(errors, map[string]string{
				"key":     err.StructField(),
				"message": err.ActualTag(),
			})
		}

		return c.JSON(http.StatusUnprocessableEntity, map[string]interface{}{
			"errors": errors,
		})
	}

	if err := (c.(*utility.CustomContext)).DB.Create(&user).Error; err != nil && strings.Contains(err.Error(), "1062") {
		return c.JSON(http.StatusConflict, err)
	}

	return c.JSON(http.StatusOK, "")
}

// UpdateUser ...
func UpdateUser(c echo.Context) error {
	user := new(model.User)
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusNotAcceptable, err)
	}

	if err := c.Validate(user); err != nil {
		errors := []map[string]string{}
		for _, err := range err.(validator.ValidationErrors) {
			errors = append(errors, map[string]string{
				"key":     err.StructField(),
				"message": err.ActualTag(),
			})
		}

		return c.JSON(http.StatusUnprocessableEntity, map[string]interface{}{
			"errors": errors,
		})
	}

	if err := (c.(*utility.CustomContext)).DB.Model(model.User{}).Select([]string{"name", "password"}).Updates(user).Error; err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, "")
}

// Login ...
func Login(c echo.Context) error {
	user := new(model.User)
	if err := c.Bind(user); err != nil {
		return c.JSON(http.StatusNotAcceptable, err)
	}

	databaseUser := new(model.User)
	(c.(*utility.CustomContext)).DB.Where(model.User{Username: user.Username}).Or(model.User{Email: user.Username}).First(&databaseUser)

	if len(databaseUser.Username) == 0 {
		return c.JSON(http.StatusUnauthorized, "User not found")
	}

	if err := bcrypt.CompareHashAndPassword([]byte(databaseUser.Password), []byte(user.Password)); err != nil {
		return c.JSON(http.StatusUnauthorized, "Wrong password")
	}

	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)
	claims["username"] = databaseUser.Username
	claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

	generatedToken, err := token.SignedString([]byte("sayakanmaba"))
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err)
	}

	return c.JSON(http.StatusOK, generatedToken)
}

// Welcome ...
func Welcome(c echo.Context) error {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	username := claims["username"].(string)
	return c.JSON(http.StatusOK, fmt.Sprintf("Welcome, %s!", username))
}

package main

import (
	"net/http"
	"os"

	"controller"
	"model"
	"utility"

	"github.com/go-playground/validator"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func main() {
	db, dbError := gorm.Open("mysql", "root:@(localhost)/ayodev?charset=utf8&parseTime=True&loc=Local")
	if dbError != nil {
		panic(dbError)
	}
	defer db.Close()

	db.AutoMigrate(&model.User{})

	e := echo.New()
	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			cc := &utility.CustomContext{c, db}
			return next(cc)
		}
	})
	e.Validator = &utility.CustomValidator{Validator: validator.New()}

	e.GET("/", func(c echo.Context) error {
		return c.JSON(http.StatusOK, "Hello World!")
	})
	e.POST("login", controller.Login)

	// p stands for protected
	p := e.Group("/protected")
	p.Use(middleware.JWT([]byte("sayakanmaba")))

	// Routing Started
	p.GET("/users/welcome", controller.Welcome)
	p.GET("/users", controller.IndexUser)
	e.POST("/users", controller.CreateUser)
	p.GET("/users/:username", controller.GetUser)

	e.Logger.Fatal(e.Start(":" + os.Getenv("PORT")))
}
